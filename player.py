# coding: utf-8
import logging

from homeassistant.components.media_player import (
    SUPPORT_PAUSE, SUPPORT_PLAY, SUPPORT_PLAY_MEDIA, SUPPORT_VOLUME_MUTE, SUPPORT_VOLUME_SET,
    SUPPORT_SELECT_SOURCE, MediaPlayerEntity, SUPPORT_STOP, SUPPORT_VOLUME_STEP)

from homeassistant.components.media_player.const import (MEDIA_TYPE_MUSIC)
import homeassistant.const as constants
import vlc

PLAYER_SUPPORT = \
    SUPPORT_STOP | SUPPORT_VOLUME_SET | SUPPORT_VOLUME_STEP | SUPPORT_VOLUME_MUTE | \
    SUPPORT_PLAY_MEDIA | SUPPORT_SELECT_SOURCE | SUPPORT_PAUSE | SUPPORT_PLAY


_LOGGER = logging.getLogger(__name__)


class Player(MediaPlayerEntity):
    def __init__(self, stations):
        self._name = 'Radio'
        self._sources = stations
        self._selected_source = list(stations.keys())[0]

        self._state = constants.STATE_PAUSED
        self._volume = None
        self._muted = None

        self._instance = vlc.Instance('')
        self._vlc = self._instance.media_player_new()

        self._volume = 0.5

    @property
    def name(self):
        return self._name

    @property
    def state(self):
        """State of the player."""
        return self._state

    @property
    def supported_features(self):
        return PLAYER_SUPPORT

    @property
    def media_content_type(self):
        """Content type of current playing media."""
        return MEDIA_TYPE_MUSIC

    @property
    def volume_level(self):
        """Volume level of the media player (0..1)."""
        return self._volume

    @property
    def is_volume_muted(self):
        """Boolean if volume is currently muted."""
        return self._muted

    @property
    def media_title(self):
        """Title of current playing media."""
        return self._selected_source if self._state == constants.STATE_PLAYING else None

    @property
    def media_image_url(self):
        return self._sources[self._selected_source].get('image', None)

    @property
    def source(self):
        """Name of the current input source."""
        return self._selected_source

    @property
    def source_list(self):
        """List of available input sources."""
        return list(self._sources.keys())

    def mute_volume(self, mute):
        """Mute the volume."""
        self._vlc.audio_set_mute(mute)
        self._muted = mute

    def set_volume_level(self, volume):
        """Set volume level, range 0..1."""
        self._vlc.audio_set_volume(int(volume * 100))
        self._volume = volume

    def media_play(self, media_type=None, media_id=None, **kwargs):
        """Send play command."""

        # set the volume because otherwise VLC might use 100% instead
        # of whatever is default
        self.set_volume_level(self._volume)

        if media_id == None:
            media_id = self._sources[self._selected_source]['url']
            media_type = MEDIA_TYPE_MUSIC

        if not media_type == MEDIA_TYPE_MUSIC:
            _LOGGER.error(
                "Invalid media type %s. Only %s is supported",
                media_type,
                MEDIA_TYPE_MUSIC,
            )
            return

        self._vlc.set_media(self._instance.media_new(media_id))
        self._vlc.play()
        self._state = constants.STATE_PLAYING

    def media_stop(self):
        """Send stop command."""
        self._vlc.stop()
        self._state = constants.STATE_PAUSED

    def media_pause(self):
        """Send pause command"""
        self.media_stop()

    def select_source(self, source):
        """Select input source."""
        self._selected_source = source
        self.media_play()
