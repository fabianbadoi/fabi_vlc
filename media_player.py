"""
Set up
"""
import logging, subprocess, os, voluptuous as vol
from time import sleep

from homeassistant.components.media_player import PLATFORM_SCHEMA
import homeassistant.helpers.config_validation as cv

from .player import Player

_LOGGER = logging.getLogger(__name__)

PLATFORM_SCHEMA = PLATFORM_SCHEMA.extend({
    vol.Required('stations', default={}): vol.Schema({
        vol.Length(min=3): vol.Schema({
            vol.Required('url'): vol.Url(),
            vol.Required('image'): cv.string
        })
    })
})

DOMAIN="media_player.fabi_vlc"

def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the media player vlc platform."""
    stations = config.get('stations', [])
    add_devices([Player(stations)])

    return True
