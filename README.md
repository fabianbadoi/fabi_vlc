# README #

VLC module for [Home Assistant](https://home-assistant.io/).

### Requirements? ###

```
pip install python-vlc
```

### How do I get set up? ###

* cd into [home assistant dir]/custom\_components/media\_player
* git clone this repo

### Configuration example ###
```yaml
media_player:
  vlc_uid: 1001
  vlc_gid: 1004 
  platform: fabi_vlc
  password: admin
  name: 'Raspberry Pi'
  stations:
    'RFI FR':
      url: 'http://live02.rfi.fr/rfimonde-64.mp3'
      image: 'http://scd.rfi.fr/sites/filesrfi/imagecache/rfi_16x9_1024_578/sites/images.rfi.fr/files/aef_image/studioRFImic_0.JPG'

```
